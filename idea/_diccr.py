""" Pseudo Interval Dual CCR DEA model Implementation
"""
import pyomo.environ as pyo
from .utils import trapezoidal_rule, calculate_lid

DICCR = pyo.AbstractModel()

DICCR.Inputs = pyo.Set()
DICCR.Outputs = pyo.Set()
DICCR.DMUs = pyo.Set()

DICCR.low = pyo.Param(domain=pyo.UnitInterval, default=0.0)
DICCR.up = pyo.Param(
    domain=pyo.UnitInterval, validate=lambda m, v: v >= m.low, default=1.0)
DICCR.bins = pyo.Param(domain=pyo.NonNegativeIntegers, default=1)

DICCR.X_mid = pyo.Param(
    DICCR.DMUs, DICCR.Inputs, domain=pyo.NonNegativeReals, default=1.0)
DICCR.X_rad = pyo.Param(
    DICCR.DMUs, DICCR.Inputs, domain=pyo.NonNegativeReals, default=0.0)
DICCR.Y_mid = pyo.Param(
    DICCR.DMUs, DICCR.Outputs, domain=pyo.NonNegativeReals, default=1.0)
DICCR.Y_rad = pyo.Param(
    DICCR.DMUs, DICCR.Outputs, domain=pyo.NonNegativeReals, default=0.0)

DICCR.lid = pyo.Param(initialize=calculate_lid)

DICCR.Partition = pyo.Set(initialize=lambda m: pyo.RangeSet(0, m.bins))
DICCR.Sigmas = pyo.Param(
    DICCR.Partition,
    initialize=lambda m, n: m.low + (m.up - m.low)*n*m.bins**(-1))
DICCR.Signs = pyo.RangeSet(-1, 1, 2)

DICCR.X = pyo.Param(
    DICCR.Signs, DICCR.DMUs, DICCR.Inputs, DICCR.Partition,
    initialize=lambda m, g, d, i, n: m.X_mid[d, i] + g * m.Sigmas[n] * m.lid * m.X_rad[d, i])
DICCR.Y = pyo.Param(
    DICCR.Signs, DICCR.DMUs, DICCR.Outputs, DICCR.Partition,
    initialize=lambda m, g, d, o, n: m.Y_mid[d, o] + g * m.Sigmas[n] * m.lid * m.Y_rad[d, o])

DICCR.theta = pyo.Var(DICCR.Partition, DICCR.DMUs, initialize=lambda m, n, d: 1.0)
DICCR.lamb = pyo.Var(
    DICCR.Partition, DICCR.DMUs, DICCR.DMUs,
    domain=pyo.NonNegativeReals,
    initialize=lambda m, n, d0, d: 1.0 if d0 == d else 0.0)
DICCR.area = pyo.Var(DICCR.DMUs)

DICCR.efficiencies_sum = pyo.Objective(rule=lambda m: pyo.summation(m.theta))

DICCR.area_calc = pyo.Constraint(
    DICCR.DMUs,
    rule=lambda m, d: trapezoidal_rule([m.theta[n, d] for n in m.Partition]) == m.area[d]
        if m.bins > 0 else m.theta[0, d] == m.area[d])

DICCR.inputs = pyo.Constraint(
    DICCR.Partition, DICCR.DMUs, DICCR.Inputs,
    rule=lambda m, n, d0, i: (
        (m.theta[n, d0] - m.lamb[n, d0, d0]) * m.X[1, d0, i, n] >=
        sum(m.lamb[n, d0, d] * m.X[-1, d, i, n] for d in m.DMUs if d0 != d)
    )
)

DICCR.outputs = pyo.Constraint(
    DICCR.Partition, DICCR.DMUs, DICCR.Outputs,
    rule=lambda m, n, d0, o: (
        (1 - m.lamb[n, d0, d0]) * m.Y[-1, d0, o, n] <=
        sum(m.lamb[n, d0, d] * m.Y[1, d, o, n] for d in m.DMUs if d0 != d)
    )
)
