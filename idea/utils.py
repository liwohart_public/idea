""" IDEA Helper Functions
"""
from typing import List as _List, Tuple as _Tuple
from pandas import DataFrame as _DataFrame
from pyomo.environ import ConcreteModel as _ConcreteModel

__all__ = ['smr', 'trapezoidal_rule', 'calculate_lid']

def smr(
        dframe: _DataFrame,
        inputs: _List[str],
        outputs: _List[str]
    ) -> _Tuple[_DataFrame, _DataFrame]:
    """ Function to extract midpoint and radius values
    """
    rads = [f'{i}_rad' for i in inputs + outputs]
    rad_map = dict(zip(rads, inputs+outputs))
    df_mid = dframe[inputs + outputs]
    df_rad = dframe[rads].rename(columns=rad_map)
    return df_mid, df_rad

def calculate_lid(model: _ConcreteModel):
    """ Function to calculate maximum tolerance
    """
    return min(
        1,
        *(
            model.X_mid[d, i] / model.X_rad[d, i] if model.X_rad[d, i] != 0 else 1
            for d in model.DMUs
            for i in model.Inputs
        ),
        *(
            model.Y_mid[d, o] / model.Y_rad[d, o] if model.Y_rad[d, o] != 0 else 1
            for d in model.DMUs
            for o in model.Outputs
        )
    )


def trapezoidal_rule(thetas: _List[float]) -> float:
    """ Numerical integration following trapezoidal rule
    """
    return (0.5 * (thetas[0] + thetas[-1]) + sum(thetas[1: -1])) / (len(thetas) - 1)
