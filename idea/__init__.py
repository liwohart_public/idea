""" IDEA Implementation
"""
from collections.abc import Iterable as _Iterable
from typing import List as _List, Dict as _Dict
from pandas import DataFrame as _DataFrame
from pyomo.environ import ConcreteModel as _ConcreteModel, AbstractModel as _AbstractModel
from ._diccr import DICCR as _DICCR
from ._ibcc import IBCC as _IBCC

__all__ = ['idea', 'utils']

_MODEL_MAP: _Dict[str, _AbstractModel] = {'ibcc' : _IBCC, 'diccr' : _DICCR}

def idea(df_mid: _DataFrame, df_rad: _DataFrame, model: str = 'ibcc',
         inputs: _List[str] = None, outputs: _List[str] = None,
         lower: float = 0.0, upper: float = 1.0, bins: int = 1) -> _ConcreteModel:
    """ IDEA Implementation
    """

    input_cols, output_cols = ([col for col in inputs if col in df_mid.columns]
                               if inputs is not None else [],
                               [col for col in outputs if col in df_mid.columns]
                               if outputs is not None else [])

    data = {'DMUs' : list(set(df_mid.index) | set(df_rad.index)),
            'lower' : {None : lower},
            'upper' : {None : upper},
            'bins' : {None : bins},
            'Inputs' : inputs,
            'X_mid' : df_mid[input_cols].stack(),
            'X_rad' : df_rad[input_cols].stack(),
            'Outputs' : outputs,
            'Y_mid' : df_mid[output_cols].stack(),
            'Y_rad' : df_rad[output_cols].stack()}

    return _MODEL_MAP[model].create_instance({None : data})
