""" Interval BCC DEA model Implementation
"""
import pyomo.environ as pyo
from .utils import trapezoidal_rule, calculate_lid

IBCC = pyo.AbstractModel()

IBCC.Inputs = pyo.Set()
IBCC.Outputs = pyo.Set()
IBCC.DMUs = pyo.Set()

IBCC.lower = pyo.Param(domain=pyo.UnitInterval, default=0.0)
IBCC.upper = pyo.Param(domain=pyo.UnitInterval, validate=lambda m, v: v >= m.lower, default=1.0)
IBCC.bins = pyo.Param(domain=pyo.NonNegativeIntegers, default=1)

IBCC.X_mid = pyo.Param(IBCC.DMUs, IBCC.Inputs, domain=pyo.NonNegativeReals, default=1.0)
IBCC.X_rad = pyo.Param(IBCC.DMUs, IBCC.Inputs, domain=pyo.NonNegativeReals, default=0.0)
IBCC.Y_mid = pyo.Param(IBCC.DMUs, IBCC.Outputs, domain=pyo.NonNegativeReals, default=1.0)
IBCC.Y_rad = pyo.Param(IBCC.DMUs, IBCC.Outputs, domain=pyo.NonNegativeReals, default=0.0)

IBCC.lid = pyo.Param(initialize=calculate_lid)

IBCC.Partition = pyo.Set(initialize=lambda m: pyo.RangeSet(0, m.bins))
IBCC.Sigmas = pyo.Param(IBCC.Partition,
                        initialize=lambda m, n: m.lower + (m.upper - m.lower)*n*m.bins**(-1)
                        if m.bins > 0 else m.lower)
IBCC.Signs = pyo.Set(initialize=lambda m: pyo.RangeSet(-1, 1, 2) if m.bins > 0 else [0])

IBCC.X = pyo.Param(IBCC.Signs, IBCC.DMUs, IBCC.Inputs, IBCC.Partition,
                   initialize=lambda m, g, d, i, n:
                   m.X_mid[d, i] + g * m.Sigmas[n] * m.lid * m.X_rad[d, i])
IBCC.Y = pyo.Param(IBCC.Signs, IBCC.DMUs, IBCC.Outputs, IBCC.Partition,
                   initialize=lambda m, g, d, o, n:
                   m.Y_mid[d, o] + g * m.Sigmas[n] * m.lid * m.Y_rad[d, o])

IBCC.theta = pyo.Var(IBCC.Partition, IBCC.DMUs, initialize=lambda m, n, d: 1.0)
IBCC.lamb = pyo.Var(IBCC.Partition, IBCC.DMUs, IBCC.DMUs,
                    domain=pyo.NonNegativeReals,
                    initialize=lambda m, n, d0, d: float(bool(d0 == d)))
IBCC.area = pyo.Var(IBCC.DMUs)

IBCC.average_efficiencie = pyo.Objective(
    rule=lambda m: pyo.summation(m.theta) / ((m.bins + 1) * len(next(m.DMUs.values()))))

IBCC.area_calc = pyo.Constraint(IBCC.DMUs,
                                rule=lambda m, d:
                                trapezoidal_rule([m.theta[n, d] for n in m.Partition]) == m.area[d]
                                if m.bins > 0 else m.theta[0, d] == m.area[d])

IBCC.lamb_sum = pyo.Constraint(IBCC.Partition, IBCC.DMUs,
                               rule=lambda m, n, d0: sum(m.lamb[n, d0, d] for d in m.DMUs) == 1.0)

IBCC.inputs = pyo.Constraint(IBCC.Partition, IBCC.DMUs, IBCC.Inputs,
                             rule=lambda m, n, d0, i: (
                                 (m.theta[n, d0] - m.lamb[n, d0, d0]) * m.X[1, d0, i, n] >=
                                 sum(m.lamb[n, d0, d] * m.X[-1, d, i, n]
                                     for d in m.DMUs if d0 != d)) if m.bins > 0 else (
                                         (m.theta[n, d0] - m.lamb[n, d0, d0]) * m.X_mid[d0, i] >=
                                         sum(m.lamb[n, d0, d] * m.X_mid[d, i]
                                             for d in m.DMUs if d0 != d)))

IBCC.outputs = pyo.Constraint(IBCC.Partition, IBCC.DMUs, IBCC.Outputs,
                              rule=lambda m, n, d0, o: (
                                  (1 - m.lamb[n, d0, d0]) * m.Y[-1, d0, o, n] <=
                                  sum(m.lamb[n, d0, d] * m.Y[1, d, o, n]
                                      for d in m.DMUs if d0 != d)) if m.bins > 0 else (
                                          (1 - m.lamb[n, d0, d0]) * m.Y_mid[d0, o] <=
                                          sum(m.lamb[n, d0, d] * m.Y_mid[d, o]
                                              for d in m.DMUs if d0 != d)))
